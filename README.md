# C'est quoi ça ?

Plugin Wordpress pour le site international qui permet de créer un PDF à partir de la page Web.
Le plugin permet de créer le PDF avec les styles CSS définis pour l'impression papier. Il n'en existait pas de satisfaisant dans Wordpress à l'époque.

# Comment ça marche ?

Le plugin utilise PhantomJs pour générer le PDF de manière suffisamment satisfaisante, inclus dans le plugin.
