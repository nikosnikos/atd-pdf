<?php
/*
 * Plugin Name: ATD PDF
 * Version: 1.0
 * Plugin URI: http://www.atd-quartmonde.org
 * Description: Create PDF documents from your WordPress pages
 * Author: ATD Fourth World forked from DK PDF from Emili Castells
 * Author URI: http://www.atd-quartmonde.org
 * Requires at least: 4.4.2
 * Tested up to: 4.4.2
 *
 * Text Domain: atd-pdf
 * Domain Path: /languages
 */
define( 'ATDPDF_VERSION', '1.0.0' );

global $os;
$os = php_uname('s');

function atdpdf_activate() {
  $upload_dir =  wp_upload_dir()['basedir'] . '/atdpdf';

  // TODO Create dir for all blogs in network case
  // Create PDF cache directory in uploads if not already
  if ( ! is_dir($upload_dir) && ! wp_mkdir_p( $upload_dir )) {
    if ( 0 === strpos( $uploads['basedir'], ABSPATH ) ) {
        $error_path = str_replace( ABSPATH, '', $uploads['basedir'] ) . $uploads['subdir'];
    } else {
        $error_path = basename( $uploads['basedir'] ) . $uploads['subdir'];
    }
?>
    <!doctype html>
    <html>
    <head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <title><?php _e( 'Unable to create directory', 'atd-pdf' ); ?></title>
    <body>
    <p><?php echo esc_html( sprintf( __( 'Unable to create directory %s. Is its parent directory writable by the server?', 'atd-pdf' ), $error_path ) ); ?></p>
    </body>
    </html>
    <?php
    exit();
  }

  add_option('atdpdf_header_image', false);
}

register_activation_hook( __FILE__, 'atdpdf_activate' );

function atdpdf_load_textdomain() {
  load_plugin_textdomain( 'atd-pdf', false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );
}

add_action( 'plugins_loaded', 'atdpdf_load_textdomain' );

function atdpdf_enqueue_styles () {
  wp_register_style( 'font-awesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css', array(), '4.6.3' );
  wp_enqueue_style( 'font-awesome' );

  wp_register_style( 'atdpdf-button', plugins_url( 'atd-pdf/css/pdfbutton.css' ), array(), ATDPDF_VERSION );
  wp_enqueue_style( 'atdpdf-button' );
}

add_action( 'wp_enqueue_scripts', 'atdpdf_enqueue_styles', 15 );

// The PDF button
function atdpdf_button( $pdfbutton_text, $pdf_cache_id, $pdfbutton_align ) {
  // Check alig is a valid text-align value
  $valid_text_align = array('right', 'center', 'left', 'justify', 'justify-all', 'start', 'end', 'match-parent', 'inherit', 'initial', 'unset');
  if (!in_array($pdfbutton_align, $valid_text_align)) {
    $pdfbutton_align = 'right';
  }

  return '<div class="atdpdf-button-container" style="text-align:' . $pdfbutton_align . ';">
    <a class="atdpdf-button" href="' . esc_url( add_query_arg( 'pdf', $pdf_cache_id ) ) . '" target="_blank" rel="alternate" type="application/pdf"><span class="atdpdf-button-icon"><i class="fa fa-file-pdf-o"></i></span>' . $pdfbutton_text . '</a>
  </div>';
}

add_filter( 'atdpdf-button', 'atdpdf_button', 10, 3 );

/**
 * Add PDF button to content
 */
function atdpdf_display_pdf_button( $content ) {
  if ( is_archive() || is_front_page() || is_home() ) {
    return $content;
  }

  global $post;
  $post_type = get_post_type( $post->ID );

  $option_post_types = get_option( 'atdpdf-post-types', array('post','page') );

  if( ! $option_post_types || ! in_array( $post_type, $option_post_types )) {
    return $content;
  }

  $pdfbutton_text = apply_filters( 'atdpdf-pdfbutton-text', __('Get this post in PDF', 'atd-pdf') );
  $pdfbutton_align = get_option( 'atdpdf-button-align', 'right' );

  $pdfbutton_html = apply_filters( 'atdpdf-button',  $pdfbutton_text, md5($content . apply_filters( 'atdpdf-getopts', '', '' )), $pdfbutton_align);

  if( get_option( 'atdpdf-button-position', 'bottom' ) === 'top') {
   return $pdfbutton_html . $content;
  } else {
   return $content . $pdfbutton_html;
  }
}

add_filter( 'the_content', 'atdpdf_display_pdf_button' );

/**
* output the pdf
*/
function atdpdf_output_pdf( $query ) {
  global $os;

  // Si c'est bien un md5
  if (preg_match('/^[a-f0-9]{32}$/', $_GET['pdf'])) {
    $pdf = $_GET['pdf'];
  }
  // Sinon c'est une tentative hack
  else {
    return;
  }

  $upload_dir =  wp_upload_dir()['basedir'] . '/atdpdf';

  if( !empty($pdf) ) {
    $pdf_cached = $upload_dir . '/' . $pdf . '.pdf';

    // Si le pdf n'existe pas on le crée
    if (! file_exists($pdf_cached)) {
      switch ($os) {
        case 'Windows NT':
        $os_path = 'win32/bin/phantomjs.exe';
        break;
        case 'Darwin':
        $os_path = 'mac/bin/phantomjs';
        break;
        case 'Linux':
        $os_path = (PHP_INT_MAX == 2147483647 ? 'linux-x86/bin/phantomjs' : 'linux-x86_64/bin/phantomjs');
        break;
      }

      $phantomjs = realpath(__DIR__ . '/phantomjs/' . $os_path);

      $result = shell_exec($phantomjs . ' ' . __DIR__ . '/atd-pdf.js ' . apply_filters( 'atdpdf-getopts', get_site_url( null, $query->request ), $pdf_cached ) . ( $os == 'Linux' ? ' 2>&1' : '' ) );

      if ($result != "Page rendered !\n" && substr($result, 0, 70) != "TypeError: undefined is not a function (evaluating 'T.canPlayType(n)')") {
        echo("<html><body><h1>".__('Error Rendering PDF', 'atd-pdf')."</h1><p>".__('Error:', 'atd-pdf')." $result</p><p>".__('Please contact the webmaster : webmaster@atd-fourthworld.org', 'atd-pdf')."</body></html>");
        exit();
      }
    }

    header('Content-Description: File Transfer');
    header('Content-Type: application/pdf');
    header('Content-Disposition: attachment; filename="'.basename($query->request).'.pdf"');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($pdf_cached));
    header('Link: <'.get_site_url( null, $query->request ).'/>; rel="canonical"');
    readfile($pdf_cached);
    exit();
  }
}

add_action( 'wp', 'atdpdf_output_pdf' );

function atdpdf_getopts($abs_url = '', $pdf_cached = '') {
  if ( empty( get_option( 'atdpdf-header-image', '') ) || empty( wp_get_attachment_url( get_option( 'atdpdf-header-image', '' ) ) ) ) {
    $first_header = '';
  } else {
    $first_header = '<div><a href="' . get_site_url() . '"><img style="height:20mm; float: left;" src="' . wp_get_attachment_url( get_option( 'atdpdf-header-image', '' ) ) . '"/></a></div>';
  }

  $footer = '<div style="height: 12mm; padding-top: 5mm; font-family: Helvetica; font-size: 7pt;"><div style="float: left; max-width: 12cm; height: 12mm; line-height: 12mm;">' . __( sprintf( 'Source: %s', '<a href="' . $abs_url . '">' . $abs_url . '</a>' ) ) . '</div><span style="float: right;">{{pageNum}}/{{numPages}}</span></div>';

  $pdfformat = array(
    'url' => $abs_url,
    'filename' => $pdf_cached,
    'paperformat' => get_option('atdpdf-paperformat', 'A4'),
    'orientation' => get_option('atdpdf-orientation', 'portrait'),
    'zoom' => 1,
    'margin' => get_option('atdpdf-margin', '15mm'),
    'header-height' => '1mm',
    'first-header-content' => $first_header,
    'header-content' => '',
    'footer-height' => '10mm',
    'first-footer-content' => $footer,
    'footer-content' => $footer,
    'additional_styles' => get_option('atdpdf-additional-styles', 'body{margin-top: 20mm;} .atdpdf-button-container{display: none;}'),
  );

  return implode(' ', array_map('escapeshellarg', $pdfformat));
}

add_filter( 'atdpdf-getopts', 'atdpdf_getopts', 10, 2 );

function atdpdf_register_settings() {
  require_once('admin/class-admin.php');
  $atdpdf_admin = ATDPDF_Admin::instance(ATDPDF_VERSION);
  $atdpdf_admin->register_settings();
}

// Register plugin settings
add_action( 'admin_init' , 'atdpdf_register_settings' );

function atdpdf_create_admin_menu() {
  require_once('admin/class-admin.php');
  $atdpdf_admin = ATDPDF_Admin::instance(ATDPDF_VERSION);

  add_options_page( __( 'ATD PDF Options', 'atd-pdf' ), 'ATD PDF', 'manage_options', $atdpdf_admin->settings_id, array( $atdpdf_admin, 'settings_page' ) );
  add_action( 'admin_enqueue_scripts', array( $atdpdf_admin, 'enqueue_admin_scripts' ) );
}

add_action('admin_menu', 'atdpdf_create_admin_menu');

/* @TODO à faire marcher... Un jour.
function atdpdf_cron_exec() {
  $all_pdfs = list_files( wp_upload_dir()['basedir'] . '/atdpdf', 1 );
  $current_time = time();

	foreach ( (array) $all_pdfs as $one_pdf ) {
		$file_age_in_seconds = $current_time - filemtime( $one_pdf );

    // 2 ans en secondes => on supprime le fichier
		if ( 63113904 < $file_age_in_seconds ) {
			unlink( $one_pdf );
		}
	}
}

add_action( 'atdpdf_cron_hook', 'atdpdf_cron_exec' );

// Ménage dans les pdfs tous les jours
if ( ! wp_next_scheduled( 'atdpdf_cron_hook' ) ) {
    wp_schedule_event( time(), 'daily', 'atdpdf_cron_hook' );
}

/**
 * Suppression du cron lors de la désactivation du plugin
function atdpdf_deactivate() {
   $timestamp = wp_next_scheduled( 'atdpdf_cron_hook' );
   wp_unschedule_event( $timestamp, 'atdpdf_cron_hook' );
}

register_deactivation_hook( __FILE__, 'atdpdf_deactivate' );
*/
