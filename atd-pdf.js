"use strict";
var page = require('webpage').create(),
  system = require('system'),
  address, output, size, orientation, zoom, margin, firstHeaderContent, firstFooterContent, headerHeight, headerContent, footerHeight, footerContent, additionalStyles;

if (system.args.length !== 14) {
  console.log('Usage: atd-pdf.js URL filename paperformat orientation zoom margin header-height header-content footer-height footer-content additional-styles');
  console.log('  paperformat examples: "5in*7.5in", "10cm*20cm", "A4", "Letter"');
  console.log('  header-content examples: "my header <span style:"float:right">{{pageNum}}/{{numPages}}</span>", "Letter"');
  phantom.exit(1);
} else {
  address = system.args[1];
  output = system.args[2];
  size = system.args[3].split('*');
  orientation = system.args[4];
  zoom = system.args[5];
  margin = system.args[6];
  headerHeight = system.args[7];
  firstHeaderContent = system.args[8] === undefined ? '' : system.args[8];
  headerContent = system.args[9] === undefined ? '' : system.args[9];
  footerHeight = system.args[10];
  firstFooterContent = system.args[11] === undefined ? '' : system.args[11];
  footerContent = system.args[12] === undefined ? '' : system.args[12];
  additionalStyles = system.args[13];

  page.paperSize = size.length === 2 ? {
    width: size[0],
    height: size[1],
    margin: margin
  } : {
    format: size[0],
    orientation: orientation,
    margin: margin,
    header: {
      height: headerHeight,
      contents: phantom.callback(function (pageNum, numPages) {
        if (pageNum === 1) {
          return (firstHeaderContent.replace('{{pageNum}}', pageNum).replace('{{numPages}}', numPages));
        }
        return (headerContent.replace('{{pageNum}}', pageNum).replace('{{numPages}}', numPages));
      })
    },
    footer: {
      height: footerHeight,
      contents: phantom.callback(function (pageNum, numPages) {
        if (pageNum === 1) {
          return (firstFooterContent.replace('{{pageNum}}', pageNum).replace('{{numPages}}', numPages));
        }
        return (footerContent.replace('{{pageNum}}', pageNum).replace('{{numPages}}', numPages));
      })
    }
  };

  page.zoomFactor = zoom;

  // BIG workaround to display images in header and footer
  // See : https://github.com/ariya/phantomjs/issues/14178
  // See also issues #12887 and #10735 on phantomjs github
  var imgRex = /<img[^>]+src="([^"]+)"/g;
  var headAndFootImgs = '';
  var match = imgRex.exec(headerContent + footerContent + firstHeaderContent + firstFooterContent);

  while (match) {
    if (match[1]) {
      headAndFootImgs += '<img style="display: none;" src="' + match[1] + '"/>';
    }
    match = imgRex.exec(headerContent + footerContent + firstHeaderContent + firstFooterContent);
  }

  page.open(address, function (status) {
    if (status !== 'success') {
      console.log('Unable to load the address!');
      phantom.exit(1);
    } else {
      // Add images to body to display in header/footer (workaround)
      // And add some styles
      page.setContent(page.content.replace('</body>', headAndFootImgs + '<style type="text/css">' + additionalStyles + '</style></body>'), address);
      // And wait until load finished to render.
      page.onLoadFinished = function (status) {
        window.setTimeout(function () {
          page.render(output);
          console.log('Page rendered !');
          phantom.exit();
        }, 60);
      };
    }
  });
}
