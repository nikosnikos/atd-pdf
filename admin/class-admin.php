<?php

/**
 *
 */
class ATDPDF_Admin
{
  /**
   * The single instance of ATD_PDF.
   * @var   object
   * @access  private
   * @since   1.0.0
   */
  private static $_instance = null;

  /**
   * The version number.
   * @var     string
   * @access  public
   * @since   1.0.0
   */
  public $version;

  private $settings = array();

  public $settings_id = 'atdpdf_settings';

  public function __construct($version) {
    $this->version = $version;
    $this->settings = $this->settings_fields();
  }

  /**
   * Main ATDPDF_Admin Instance
   *
   * Ensures only one instance of ATDPDF_Admin is loaded or can be loaded.
   *
   * @since 1.0.0
   * @static
   * @see ATDPDF_Admin()
   * @return Main ATDPDF_Admin instance
   */
  public static function instance ( $version = '1.0.0' ) {
    if ( is_null( self::$_instance ) ) {
      self::$_instance = new self( $version );
    }
    return self::$_instance;
  } // End instance ()

  /**
   * Build settings fields
   * @return array Fields to be displayed on settings page
   */
  private function settings_fields () {
    $settings = array();

    $settings['atdpdf_pdf'] = array(
      'title'          => __( 'PDF Output', 'atd-pdf' ),
      'description'      => __( 'The PDF document format and extra settings.', 'atd-pdf' ),
      'fields'        => array(
        array(
          'id'       => 'header-image',
          'label'      => __( 'Header image' , 'atd-pdf' ),
          'description'  => __( 'This image will appear on the top left of the PDF Document', 'atd-pdf' ),
          'type'      => 'image',
          'default'    => '',
          'placeholder'  => ''
        ),
        array(
          'id'       => 'paperformat',
          'label'      => __( 'Page format' , 'atd-pdf' ),
          'type'      => 'radio',
          'options'    => array( 'A3' => __( 'A3' , 'atd-pdf' ), 'A4' => __( 'A4' , 'atd-pdf' ), 'A5' => __( 'A5' , 'atd-pdf' ), 'Legal' => __( 'Legal' , 'atd-pdf' ), 'Letter' => __( 'Letter' , 'atd-pdf' ), 'Tabloid' => __( 'Tabloid' , 'atd-pdf' ) ),
          'default'    => 'A4'
        ),
        array(
          'id'       => 'orientation',
          'label'      => __( 'Page orientation' , 'atd-pdf' ),
          'type'      => 'radio',
          'options'    => array( 'portrait' => __( 'portrait' , 'atd-pdf' ), 'landscape' => __( 'landscape' , 'atd-pdf' ) ),
          'default'    => 'portrait'
        ),
        array(
          'id'       => 'margin',
          'label'      => __( 'Page margin' , 'atd-pdf' ),
          'description'  => __( 'Page margin in millimeters', 'atd-pdf' ),
          'type'      => 'number',
          'default'    => '15',
        ),
        array(
          'id'       => 'additional-styles',
          'label'      => __( 'Additional style' , 'atd-pdf' ),
          'description'  => __( 'Valid CSS properties', 'atd-pdf' ),
          'type'      => 'textarea',
          'default'    => 'body{margin-top: 20mm;} .atdpdf-button-container{display: none;}',
        ),
      )
    );

    $settings['atdpdf_button'] = array(
      'title'          => __( 'PDF Button', 'atd-pdf' ),
      'description'      => __( 'How to display PDF button in posts.', 'atd-pdf' ),
      'fields'        => array(
        array(
          'id'       => 'button-align',
          'label'      => __( 'Button alignement', 'atd-pdf' ),
          'description'  => __( 'How to align PDF button', 'atd-pdf' ),
          'type'      => 'radio',
          'options'    => array( 'left' => 'Left', 'center' => 'Center', 'right' => 'Right' ),
          'default'    => 'right'
        ),
        array(
          'id'       => 'button-position',
          'label'      => __( 'Button Position', 'atd-pdf' ),
          'description'  => __( 'Where to place the button', 'atd-pdf' ),
          'type'      => 'radio',
          'options'    => array( 'top' => 'Top', 'bottom' => 'Bottom' ),
          'default'    => 'bottom'
        ),
        array(
          'id' 			=> 'post-types',
          'label'			=> __( 'Post types', 'atd-pdf' ),
          'description'	=> __( 'Post types where PDF button appears.', 'atd-pdf' ),
          'type'			=> 'checkbox_multi',
          'options'		=> get_post_types( array( 'public'   => true ) ),
          'default'		=> array( 'post', 'page' )
        ),
      )
    );

    $settings = apply_filters( 'atdpdf-settings-fields', $settings );

    return $settings;
  }

  function settings_section ( $section ) {
    $html = '<p> ' . $this->settings[ $section['id'] ]['description'] . '</p>' . "\n";
    echo $html;
  }

  /**
   * Load settings page content
   * @return void
   */
  public function settings_page () {

    // Build page HTML
    $html = '<div class="wrap" id="' . $this->settings_id . '">' . "\n";
      $html .= '<h2>' . __( 'ATD PDF Settings' , 'atd-pdf' ) . '</h2>' . "\n";

      $tab = '';
      if ( isset( $_GET['tab'] ) && $_GET['tab'] ) {
        $tab .= $_GET['tab'];
      }

      $settings = $this->settings;
      // Show page tabs
      if ( is_array( $settings ) && 1 < count( $settings ) ) {

        $html .= '<h2 class="nav-tab-wrapper">' . "\n";

        $c = 0;
        foreach ( $settings as $section => $data ) {

          // Set tab class
          $class = 'nav-tab';
          if ( ! isset( $_GET['tab'] ) ) {
            if ( 0 == $c ) {
              $class .= ' nav-tab-active';
            }
          } else {
            if ( isset( $_GET['tab'] ) && $section == $_GET['tab'] ) {
              $class .= ' nav-tab-active';
            }
          }

          // Set tab link
          $tab_link = add_query_arg( array( 'tab' => $section ) );
          if ( isset( $_GET['settings-updated'] ) ) {
            $tab_link = remove_query_arg( 'settings-updated', $tab_link );
          }

          // Output tab
          $html .= '<a href="' . $tab_link . '" class="' . esc_attr( $class ) . '">' . esc_html( $data['title'] ) . '</a>' . "\n";

          ++$c;
        }

        $html .= '</h2>' . "\n";
      }

      $html .= '<form method="post" action="options.php" enctype="multipart/form-data">' . "\n";

        // Get settings fields
        ob_start();
        settings_fields( $this->settings_id );
        do_settings_sections( $this->settings_id );
        $html .= ob_get_clean();

        $html .= '<p class="submit">' . "\n";
          $html .= '<input type="hidden" name="tab" value="' . esc_attr( $tab ) . '" />' . "\n";
          $html .= '<input name="Submit" type="submit" class="button-primary" value="' . esc_attr( __( 'Save Settings' , 'atd-pdf' ) ) . '" />' . "\n";
        $html .= '</p>' . "\n";
      $html .= '</form>' . "\n";
    $html .= '</div>' . "\n";

    echo $html;
  }

  public function enqueue_admin_scripts() {
    // We're including the WP media scripts here because they're needed for the image upload field
    wp_enqueue_media();

    wp_register_script( 'atdpdf-settings-js', plugin_dir_url( __FILE__ ) . 'settings.js', array( 'jquery' ), $this->version );
    wp_enqueue_script( 'atdpdf-settings-js' );
  }

  /**
   * Register plugin settings
   * @return void
   */
  public function register_settings () {
    $settings = $this->settings;

    if ( is_array( $settings ) ) {

      // Check posted/selected tab
      $current_section = '';
      if ( isset( $_POST['tab'] ) && $_POST['tab'] ) {
        $current_section = $_POST['tab'];
      } else {
        if ( isset( $_GET['tab'] ) && $_GET['tab'] ) {
          $current_section = $_GET['tab'];
        }
      }

      foreach ( $settings as $section => $data ) {

        if ( $current_section && $current_section != $section ) continue;

        // Add section to page
        add_settings_section( $section, $data['title'], array( $this, 'settings_section' ), $this->settings_id );

        foreach ( $data['fields'] as $field ) {

          // Validation callback for field
          $validation = '';
          if ( isset( $field['callback'] ) ) {
            $validation = $field['callback'];
          }

          // Register field
          $option_name = 'atdpdf-' . $field['id'];
          register_setting( $this->settings_id, $option_name, $validation );

          require_once('class-admin-api.php');
          // Add field to page
          add_settings_field( $field['id'], $field['label'], array( ATDPDF_Admin_API, 'display_field' ), $this->settings_id, $section, array( 'field' => $field, 'prefix' => 'atdpdf' ) );
        }

        if ( ! $current_section ) break;
      }
    }
  }
}
